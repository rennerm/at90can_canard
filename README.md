# AT90CAN128 canard implementation (V0.3.1)

## Info

Running [libcanard](https://github.com/UAVCAN/libcanard) on 8-bit AVR microcontroller AT90CAN128 (Olimex AVR-CAN developer board)

Depends on Eclipse with [AVR Eclipse Plugin](https://marketplace.eclipse.org/content/avr-eclipse-plugin)

Try this installation guide [AVR in ECLIPSE](http://www.instructables.com/id/How-to-get-started-with-Eclipse-and-AVR/step3/Make-a-new-project/)

AVR Dragon is used as a programmer

## Installation

### required software

with your package manager install `sudo apt-get install`
* gcc-avr
* binutils-avr
* avr-libc
* avrdude
* eclipse IDE (tested on Keppler)

### required hardware
* AVR Dragon loader

### Steps

1. checkout the repo:
`git clone https://rennerm@bitbucket.org/rennerm/at90can_canard.git`

2. initialise submodules:
`git submodule update --init --recursive`

3. precompile can lowlevel library:
go to `lib/libcanard/drivers/avr` and run  
`$ cmake .`  
`$ make`

4. setup Eclipse:
eclipse->help->Install New Software
Add... -> http://avr-eclipse.sourceforge.net/updatesite/
select AVR Eclipse Plugin. Install...

5. Import Project:
eclipse->file->Import...->Exisiting Projects Into Workspace
compile!!!

6. setup the AVR Dragon:
eclipse: select project settings
AVR->AVRDude->Tab:Programmer->New Configuration->choose in Programmer Hardware: "ATMEL AVR Dragon in JTAG Mode"

7. setup udev rules for AVR Dragon (was required on Ubuntu 14.04):
    Create new file /etc/udev/rules.d/60-avrisp.rules:
```
SUBSYSTEM!="usb", ACTION!="add", GOTO="avrisp_end"

#A tmel Corp. JTAG ICE mkII
ATTR{idVendor}=="03eb", ATTR{idProduct}=="2103", MODE="660", GROUP="dialout"
# Atmel Corp. AVRISP mkII
ATTR{idVendor}=="03eb", ATTR{idProduct}=="2104", MODE="660", GROUP="dialout"
# Atmel Corp. Dragon
ATTR{idVendor}=="03eb", ATTR{idProduct}=="2107", MODE="660", GROUP="dialout"

LABEL="avrisp_end"
```

Check you're in the "dialout" group
`groups`
    
Restart udev
`sudo service udev restart`

FLASH:
Press Button in Eclipse ("AVR" with green arrow)