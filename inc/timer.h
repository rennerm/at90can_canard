/*
 * timer.h
 *
 *  Created on: Jul 12, 2016
 *      Author: blockmurder
 */

#ifndef TIMER_H_
#define TIMER_H_

void timer_init(void);
uint64_t getMonotonicTimestampUSec(void);

#endif /* TIMER_H_ */
