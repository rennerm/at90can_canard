/****************************************************************************
* Included Files
****************************************************************************/
#include <avr/io.h>
#include <stdio.h>
#include <stdbool.h>
#include <avr/interrupt.h>

/****************************************************************************
* Pre-processor Definitions
****************************************************************************/
#define BAUD 115200
//#define MYUBRR ((F_CPU/(16*BAUD))-1) // this does not work!!
#define MYUBRR 8 // round(F_CPU/(16*BAUD))-1 must calculate with floats!!
/****************************************************************************
* Private Types
****************************************************************************/

/****************************************************************************
* Private Function Prototypes
****************************************************************************/
char usart_getchar( void );
void usart_putchar( char data );
void usart_pstr (char *s);
unsigned char usart_kbhit(void);
int usart_putchar_printf(char var, FILE *stream);

/****************************************************************************
* Private Data
****************************************************************************/
static FILE mystdout = FDEV_SETUP_STREAM(usart_putchar_printf, NULL, _FDEV_SETUP_WRITE);

/****************************************************************************
* Public Data
****************************************************************************/

/****************************************************************************
* Private Functions
****************************************************************************/

/****************************************************************************
* Public Functions
****************************************************************************/
void usart_init(void)
{
    // Set baud rate
    UBRR0H = (uint8_t)(MYUBRR>>8);
    UBRR0L = (uint8_t)MYUBRR;
    // Usart Speed divider U2X0
    //UCSR0A = (1<<U2X);
    // Enable receiver and transmitter
    UCSR0B = (1<<RXEN0)|(1<<TXEN0);
    // Set frame format: asyn, 8data, 1stop bit, no parity
    UCSR0C = (0<<UMSEL0)|(3<<UCSZ0)|(0<<USBS0)|(0<<UPM0);

    stdout = &mystdout;
}

void usart_putchar(char data)
{
    // Wait for empty transmit buffer
    while ( !(UCSR0A & (_BV(UDRE0))) );
    // Start transmission
    UDR0 = data;
}

char usart_getchar(void)
{
    // Wait for incoming data
    while ( !(UCSR0A & (_BV(RXC0))) );
    // Return the data
    return UDR0;
}

unsigned char usart_kbhit(void)
{
    //return nonzero if char waiting polled version
    unsigned char b;
    b=0;
    if(UCSR0A & (1<<RXC0)) b=1;
    return b;
}

void usart_pstr(char *s)
{
    // loop through entire string
    while (*s) {
        usart_putchar(*s);
        s++;
    }
}

int usart_putchar_printf(char var, FILE *stream)
{
    // translate \n to \r for br@y++ terminal
    if (var == '\n') usart_putchar('\r');
    usart_putchar(var);
    return 0;
}
