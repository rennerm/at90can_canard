/*  Sample program for Olimex AVR-CAN with AT90CAN128 processor
 *  Libcanard example 0.2
 *
 *  avr-gcc (GCC) 4.9.2
 *  avr-libc 3.5.0-1
 */

/****************************************************************************
 * Included Files
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include <avr/io.h>
#include <uart.h>
#include <timer.h>

#include <canard.h>
#include <canard_avr.h>

/****************************************************************************
* Pre-processor Definitions
****************************************************************************/
//#define CONFIG_DEBUG_CAN                                            1

/*
* Application constants
*/
#define CAN_BITRATE                                                 500000UL
#define PREFERRED_NODE_ID                                           11
#define APP_VERSION_MAJOR                                           1
#define APP_VERSION_MINOR                                           0
#define APP_NODE_NAME                                               "org.uavcan.libcanard.avr.demo2"
#define GIT_HASH                                                    0xb28bf6ac
#define NODE_MEM_POOL_SIZE                                          1024

/*
* Some useful constants defined by the UAVCAN specification.
* Data type signature values can be easily obtained with the script show_data_type_info.py
*/
#define UAVCAN_NODE_ID_ALLOCATION_DATA_TYPE_ID                      1
#define UAVCAN_NODE_ID_ALLOCATION_DATA_TYPE_SIGNATURE               0x0b2a812620a11d40
#define UAVCAN_NODE_ID_ALLOCATION_RANDOM_TIMEOUT_RANGE_USEC         400000UL
#define UAVCAN_NODE_ID_ALLOCATION_REQUEST_DELAY_OFFSET_USEC         600000UL

#define UAVCAN_NODE_STATUS_MESSAGE_SIZE                             7
#define UAVCAN_NODE_STATUS_DATA_TYPE_ID                             341
#define UAVCAN_NODE_STATUS_DATA_TYPE_SIGNATURE                      0x0f0868d0c1a7c6f1

#define UAVCAN_NODE_HEALTH_OK                                       0
#define UAVCAN_NODE_HEALTH_WARNING                                  1
#define UAVCAN_NODE_HEALTH_ERROR                                    2
#define UAVCAN_NODE_HEALTH_CRITICAL                                 3

#define UAVCAN_NODE_MODE_OPERATIONAL                                0
#define UAVCAN_NODE_MODE_INITIALIZATION                             1

/* Known Message Definitions and Types */
#define UAVCAN_GET_NODE_INFO_RESPONSE_MAX_SIZE                      ((3015 + 7) / 8)
#define UAVCAN_GET_NODE_INFO_DATA_TYPE_SIGNATURE                    0xee468a8121c46a9e
#define UAVCAN_GET_NODE_INFO_DATA_TYPE_ID                           1

#define UAVCAN_CUSTOMMESSAGES_GET_CIRCUITSTATUS_TYPE_ID                93
#define UAVCAN_CUSTOMMESSAGES_GET_CIRCUITSTATUS_TYPE_SIGNATURE         0x4d08bc1e5f29e3af
#define UAVCAN_CUSTOMMESSAGES_GET_CIRCUITSTATUS_RESPONSE_MESSAGE_SIZE  32

#define UAVCAN_CUSTOMMESSAGES_SET_PINOUT_TYPE_ID                    94
#define UAVCAN_CUSTOMMESSAGES_SET_PINOUT_TYPE_SIGNATURE             0xc893abac15d8eada
#define UAVCAN_CUSTOMMESSAGES_SET_PINOUT_MESSAGE_SIZE               2

#define UAVCAN_CUSTOMMESSAGES_GET_PINOUTSTATUS_TYPE_ID                    95
#define UAVCAN_CUSTOMMESSAGES_GET_PINOUTSTATUS_TYPE_SIGNATURE             0xbc45a3fe19fbd85c
#define UAVCAN_CUSTOMMESSAGES_GET_PINOUTSTATUS_RESPONSE_MESSAGE_SIZE      2

typedef struct {
  uint32_t voltage_mv_5V;
  uint32_t voltage_mv_12V;
  uint32_t voltage_mv_15V;
  uint32_t voltage_mv_48V;
  int32_t current_ma_5V;
  int32_t current_ma_12V;
  int32_t current_ma_15V;
  int32_t current_ma_48V;
} UAVCANPowerCircuitStatus;

typedef struct {
  uint8_t pin_id;
  uint8_t pin_cmd;
} UAVCANSetPinOutMsgRequest;

typedef struct {
  uint16_t pin_state;
} UAVCANSetPinOutMsgResponse;
#define UNIQUE_ID_LENGTH_BYTES                                      16
#define FACTOR_1S   (uint64_t)(1000000)

/****************************************************************************
* Private Types
****************************************************************************/

/****************************************************************************
* Private Function Prototypes
****************************************************************************/

/****************************************************************************
* Private Data
****************************************************************************/
/*
 * Library instance.
 * In simple applications it makes sense to make it static, but it is not necessary.
 */
static CanardInstance canard;                       ///< The library instance
static uint8_t canard_memory_pool[NODE_MEM_POOL_SIZE];            ///< Arena for memory allocation, used by the library

static uint8_t unique_id[UNIQUE_ID_LENGTH_BYTES] = { 0x00, 0x00, 0x00, 0x00,
                                                     0x00, 0x00, 0x00, 0x00,
                                                     0x00, 0x00, 0x00, 0x00,
                                                     0x00, 0x00, 0x01, 0x00 };

/*
 * Variables used for dynamic node ID allocation.
 * RTFM at http://uavcan.org/Specification/6._Application_level_functions/#dynamic-node-id-allocation
 */
static uint64_t send_next_node_id_allocation_request_at;    ///< When the next node ID allocation request should be sent
static uint8_t node_id_allocation_unique_id_offset;         ///< Depends on the stage of the next request

/*
 * Node status variables
 */
static uint8_t node_health = UAVCAN_NODE_HEALTH_OK;
static uint8_t node_mode   = UAVCAN_NODE_MODE_INITIALIZATION;

UAVCANPowerCircuitStatus power_circuit_status;
/****************************************************************************
* Public Data
****************************************************************************/

/****************************************************************************
* Private Functions
****************************************************************************/

void PORT_init()
{
  // only init the ports we need to

  PORTE = 0b00000000;
  DDRE = 0b00010000;    //Led set as output (Bit4 = 1) , Button set as input(Bit 5 = 0)
}

void LED_ON() {
  PORTE = PORTE & 0b11101111;   //led on (Bit4 = 0)
}

void LED_OFF() {
  PORTE = PORTE | 0b00010000;   //led off (Bit4 = 1)
}

void LED_TOGGLE() {
  PORTE = PORTE ^ 0b00010000;
}

uint8_t LED_Status() {
  if (PORTE & 0b00010000)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

unsigned short get_seed()
{
   unsigned short seed = 0;
   unsigned short *p = (unsigned short*) (RAMEND+1);
   extern unsigned short __heap_start;

   while (p >= &__heap_start + 1)
      seed ^= * (--p);

   return seed;
}

/**
 * Returns a pseudo random float in the range [0, 1].
 */
float getRandomFloat(void)
{
    static bool initialized = false;
    if (!initialized)                   // This is not thread safe, but a race condition here is not harmful.
    {
        initialized = true;
        srand(get_seed());
    }
    // coverity[dont_call]
    return (float)rand() / (float)RAND_MAX;
}

void makeNodeStatusMessage(uint8_t buffer[UAVCAN_NODE_STATUS_MESSAGE_SIZE])
{
    memset(buffer, 0, UAVCAN_NODE_STATUS_MESSAGE_SIZE);

    static uint32_t started_at_sec = 0;
    if (started_at_sec == 0)
    {
        started_at_sec = (uint32_t)(getMonotonicTimestampUSec() / FACTOR_1S);
    }

    const uint32_t uptime_sec = (uint32_t)((getMonotonicTimestampUSec() / FACTOR_1S) - started_at_sec);

    /*
     * Here we're using the helper for demonstrational purposes; in this simple case it could be preferred to
     * encode the values manually.
     */
    canardEncodeScalar(buffer,  0, 32, &uptime_sec);
    canardEncodeScalar(buffer, 32,  2, &node_health);
    canardEncodeScalar(buffer, 34,  3, &node_mode);
}

/**
 * This callback is invoked by the library when a new message or request or response is received.
 */
static void onTransferReceived(CanardInstance* ins,
                               CanardRxTransfer* transfer)
{
    /*
     * Dynamic node ID allocation protocol.
     * Taking this branch only if we don't have a node ID, ignoring otherwise.
     */
    if ((canardGetLocalNodeID(ins) == CANARD_BROADCAST_NODE_ID) &&
        (transfer->transfer_type == CanardTransferTypeBroadcast) &&
        (transfer->data_type_id == UAVCAN_NODE_ID_ALLOCATION_DATA_TYPE_ID))
    {
        // Rule C - updating the randomized time interval
        send_next_node_id_allocation_request_at =
            getMonotonicTimestampUSec() + UAVCAN_NODE_ID_ALLOCATION_REQUEST_DELAY_OFFSET_USEC +
            (uint64_t)(getRandomFloat() * UAVCAN_NODE_ID_ALLOCATION_RANDOM_TIMEOUT_RANGE_USEC);

        if (transfer->source_node_id == CANARD_BROADCAST_NODE_ID)
        {
            puts("Allocation request from another allocatee");
            node_id_allocation_unique_id_offset = 0;
            return;
        }

        // Copying the unique ID from the message
        static const unsigned UniqueIDBitOffset = 8;
        uint8_t received_unique_id[UNIQUE_ID_LENGTH_BYTES];
        uint8_t received_unique_id_len = 0;
        for (; received_unique_id_len < (transfer->payload_len - (UniqueIDBitOffset / 8U)); received_unique_id_len++)
        {
            assert(received_unique_id_len < UNIQUE_ID_LENGTH_BYTES);
            const uint8_t bit_offset = (uint8_t)(UniqueIDBitOffset + received_unique_id_len * 8U);
            // not sure whether this really works --> we have twice the byte offset?!?!
            (void) canardDecodeScalar(transfer, bit_offset, 8, false, &received_unique_id[received_unique_id_len]);
        }

        // Matching the received UID against the local one
        if (memcmp(received_unique_id, unique_id, received_unique_id_len) != 0)
        {
#ifdef CONFIG_DEBUG_CAN
            printf("Mismatching allocation response from %d:", transfer->source_node_id);
            for (int i = 0; i < received_unique_id_len; i++)
            {
                printf(" %02x/%02x", received_unique_id[i], unique_id[i]);
            }
            puts("");
#endif
            node_id_allocation_unique_id_offset = 0;
            return;         // No match, return
        }

        if (received_unique_id_len < UNIQUE_ID_LENGTH_BYTES)
        {
            // The allocator has confirmed part of unique ID, switching to the next stage and updating the timeout.
            node_id_allocation_unique_id_offset = received_unique_id_len;
            send_next_node_id_allocation_request_at -= UAVCAN_NODE_ID_ALLOCATION_REQUEST_DELAY_OFFSET_USEC;

#ifdef CONFIG_DEBUG_CAN
            printf("Matching allocation response from %d offset %d\n",
                   transfer->source_node_id, node_id_allocation_unique_id_offset);
#endif
        }
        else
        {
            // Allocation complete - copying the allocated node ID from the message
            uint8_t allocated_node_id = 0;
            (void) canardDecodeScalar(transfer, 0, 7, false, &allocated_node_id);
            assert(allocated_node_id <= 127);

            canardSetLocalNodeID(ins, allocated_node_id);
            printf("Node ID %d allocated by %d\n", allocated_node_id, transfer->source_node_id);
        }
    }

    /*
     * simply acknowledge the heartbeats from other nodes, that's all
     */
    if ((transfer->transfer_type == CanardTransferTypeBroadcast) &&
        (transfer->data_type_id == UAVCAN_NODE_STATUS_DATA_TYPE_ID))
    {
      // do nothing
      return;
    }
    /*
     * This acts as a server for the Node Info Data Type
     */
    if ((transfer->transfer_type == CanardTransferTypeRequest) &&
        (transfer->data_type_id == UAVCAN_GET_NODE_INFO_DATA_TYPE_ID))
    {
#ifdef CONFIG_DEBUG_CAN
        printf("GetNodeInfo request from %d\n", transfer->source_node_id);
#endif
        uint8_t buffer[UAVCAN_GET_NODE_INFO_RESPONSE_MAX_SIZE];
        memset(buffer, 0, UAVCAN_GET_NODE_INFO_RESPONSE_MAX_SIZE);

        // NodeStatus
        makeNodeStatusMessage(buffer);

        // SoftwareVersion
        buffer[7] = APP_VERSION_MAJOR;
        buffer[8] = APP_VERSION_MINOR;
        buffer[9] = 1;                          // Optional field flags, VCS commit is set
        uint32_t u32 = GIT_HASH;
        canardEncodeScalar(buffer, 80, 32, &u32);
        // Image CRC skipped

        // HardwareVersion
        // Major skipped
        // Minor skipped
        memcpy(&buffer[24], unique_id, UNIQUE_ID_LENGTH_BYTES);
        // Certificate of authenticity skipped

        // Name
        const size_t name_len = strlen(APP_NODE_NAME);
        memcpy(&buffer[41], APP_NODE_NAME, name_len);

        const size_t total_size = 41 + name_len;

        /*
         * Transmitting; in this case we don't have to release the payload because it's empty anyway.
         */
        const int resp_res = canardRequestOrRespond(ins,
                                                    transfer->source_node_id,
                                                    UAVCAN_GET_NODE_INFO_DATA_TYPE_SIGNATURE,
                                                    UAVCAN_GET_NODE_INFO_DATA_TYPE_ID,
                                                    &transfer->transfer_id,
                                                    transfer->priority,
                                                    CanardResponse,
                                                    &buffer[0],
                                                    (uint16_t)total_size);
        if (resp_res <= 0)
        {
            printf("Could not respond to GetNodeInfo; error %d\n", resp_res);
        }
    }

    /*
     * this acts as a Server for a GetCircuitStatus request,
     */
    if ((transfer->transfer_type == CanardTransferTypeRequest) &&
        (transfer->data_type_id == UAVCAN_CUSTOMMESSAGES_GET_CIRCUITSTATUS_TYPE_ID))
    {
#ifdef CONFIG_DEBUG_CAN
        printf("got circuitstatus request from %d\n", transfer->source_node_id);
#endif
        /*
         * Transmitting; in this case we don't have to release the payload because it's empty anyway.
         */
        const int resp_res = canardRequestOrRespond(ins,
                                                    transfer->source_node_id,
                                                    UAVCAN_CUSTOMMESSAGES_GET_CIRCUITSTATUS_TYPE_SIGNATURE,
                                                    UAVCAN_CUSTOMMESSAGES_GET_CIRCUITSTATUS_TYPE_ID,
                                                    &transfer->transfer_id,
                                                    transfer->priority,
                                                    CanardResponse,
                                                    &power_circuit_status,
                                                    UAVCAN_CUSTOMMESSAGES_GET_CIRCUITSTATUS_RESPONSE_MESSAGE_SIZE);
        if (resp_res <= 0)
        {
            printf("Could not respond to GetNodeInfo; error %d\n", resp_res);
        }
    }
    /*
     * this acts as a Server for a SetPinOut request,
     */
    if ((transfer->transfer_type == CanardTransferTypeRequest) &&
        (transfer->data_type_id == UAVCAN_CUSTOMMESSAGES_SET_PINOUT_TYPE_ID))
    {
        UAVCANSetPinOutMsgRequest request;
        memcpy(&request,transfer->payload_head,sizeof(request));

#ifdef CONFIG_DEBUG_CAN
        printf("SetPinOut src: %d, pin: %d, state %d\n", transfer->source_node_id,request.pin_id,request.pin_state);
#endif
        // React on Message
        // ignore LED NR
        if (request.pin_cmd == 0)
        {
          LED_ON(); // LED is on when pin is low...
        }
        else if (request.pin_cmd == 1)
        {
          LED_OFF();
        }
        else if (request.pin_cmd == 2)
        {
          LED_TOGGLE();
        }
        /*
         * Transmitting; in this case we don't have to release the payload because it's empty anyway.
         */

        UAVCANSetPinOutMsgResponse response;
        response.pin_state = LED_Status(); // set the LED status of the correct bit!!
        const int resp_res = canardRequestOrRespond(ins,
                                                    transfer->source_node_id,
                                                    UAVCAN_CUSTOMMESSAGES_SET_PINOUT_TYPE_SIGNATURE,
                                                    UAVCAN_CUSTOMMESSAGES_SET_PINOUT_TYPE_ID,
                                                    &transfer->transfer_id,
                                                    transfer->priority,
                                                    CanardResponse,
                                                    &response,
                                                    UAVCAN_CUSTOMMESSAGES_SET_PINOUT_MESSAGE_SIZE);
        if (resp_res <= 0)
        {
            printf("Could not respond to GetNodeInfo; error %d\n", resp_res);
        }
    }
    /*
     * this acts as a Server for a GetPinOutStatus request,
     */
    if ((transfer->transfer_type == CanardTransferTypeRequest) &&
            (transfer->data_type_id == UAVCAN_CUSTOMMESSAGES_GET_PINOUTSTATUS_TYPE_ID))
        {
    #ifdef CONFIG_DEBUG_CAN
            printf("GetPinoutStatus src: %d, pin: %d, state %d\n", transfer->source_node_id,request.pin_id,request.pin_state);
    #endif
            /*
             * Transmitting; in this case we don't have to release the payload because it's empty anyway.
             */

            UAVCANSetPinOutMsgResponse response;
            response.pin_state = LED_Status();
            const int resp_res = canardRequestOrRespond(ins,
                                                        transfer->source_node_id,
                                                        UAVCAN_CUSTOMMESSAGES_GET_PINOUTSTATUS_TYPE_SIGNATURE,
                                                        UAVCAN_CUSTOMMESSAGES_GET_PINOUTSTATUS_TYPE_ID,
                                                        &transfer->transfer_id,
                                                        transfer->priority,
                                                        CanardResponse,
                                                        &response,
                                                        UAVCAN_CUSTOMMESSAGES_SET_PINOUT_MESSAGE_SIZE);
            if (resp_res <= 0)
            {
                printf("Could not respond to GetNodeInfo; error %d\n", resp_res);
            }
        }
    /*
     * just to see if responses pass through filters
     */
    if((transfer->transfer_type == CanardTransferTypeResponse) &&
        (transfer->data_type_id == UAVCAN_GET_NODE_INFO_DATA_TYPE_ID) &&
        (transfer->source_node_id == 1))
    {
#ifdef CONFIG_DEBUG_CAN
        printf("Response from %u received!\n",transfer->source_node_id);
#endif
    }
}

/**
 * This callback is invoked by the library when it detects beginning of a new transfer on the bus that can be received
 * by the local node.
 * If the callback returns true, the library will receive the transfer.
 * If the callback returns false, the library will ignore the transfer.
 * All transfers that are addressed to other nodes are always ignored.
 */
static bool shouldAcceptTransfer(const CanardInstance* ins,
                                 uint64_t* out_data_type_signature,
                                 uint16_t data_type_id,
                                 CanardTransferType transfer_type,
                                 uint8_t source_node_id)
{
    (void)source_node_id;

    if (canardGetLocalNodeID(ins) == CANARD_BROADCAST_NODE_ID)
    {
        /*
         * If we're in the process of allocation of dynamic node ID, accept only relevant transfers.
         */
        if ((transfer_type == CanardTransferTypeBroadcast) &&
            (data_type_id == UAVCAN_NODE_ID_ALLOCATION_DATA_TYPE_ID))
        {
            *out_data_type_signature = UAVCAN_NODE_ID_ALLOCATION_DATA_TYPE_SIGNATURE;
            return true;
        }
    }
    else
    {
      if (transfer_type == CanardTransferTypeBroadcast)
      {
        if (data_type_id == UAVCAN_NODE_STATUS_DATA_TYPE_ID)
        {
          *out_data_type_signature = UAVCAN_NODE_STATUS_DATA_TYPE_SIGNATURE;
          return true;
        }
      }
      else if (transfer_type == CanardTransferTypeRequest)
      {
        if (data_type_id == UAVCAN_GET_NODE_INFO_DATA_TYPE_ID)
        {
          *out_data_type_signature = UAVCAN_GET_NODE_INFO_DATA_TYPE_SIGNATURE;
          return true;
        }
        if (data_type_id == UAVCAN_CUSTOMMESSAGES_GET_CIRCUITSTATUS_TYPE_ID)
        {
            *out_data_type_signature = UAVCAN_CUSTOMMESSAGES_GET_CIRCUITSTATUS_TYPE_SIGNATURE;
            return true;
        }
        if (data_type_id == UAVCAN_CUSTOMMESSAGES_GET_PINOUTSTATUS_TYPE_ID)
        {
            *out_data_type_signature = UAVCAN_CUSTOMMESSAGES_GET_PINOUTSTATUS_TYPE_SIGNATURE;
            return true;
        }
        if (data_type_id == UAVCAN_CUSTOMMESSAGES_SET_PINOUT_TYPE_ID)
        {
            *out_data_type_signature = UAVCAN_CUSTOMMESSAGES_SET_PINOUT_TYPE_SIGNATURE;
            return true;
        }
      }
    }

    return false;
}

/**
 * This function is called at 1 Hz rate from the main loop.
 */
void process1HzTasks(uint64_t timestamp_usec)
{
    /*
     * Purging transfers that are no longer transmitted. This will occasionally free up some memory.
     */
    canardCleanupStaleTransfers(&canard, timestamp_usec);

    /*
     * Printing the memory usage statistics.
     */
    {
        const CanardPoolAllocatorStatistics stats = canardGetPoolAllocatorStatistics(&canard);
        const unsigned peak_percent = 100U * stats.peak_usage_blocks / stats.capacity_blocks;
#ifdef CONFIG_DEBUG_CAN
        printf("Memory pool stats: capacity %u blocks, usage %u blocks, peak usage %u blocks (%u%%)\n",
               stats.capacity_blocks, stats.current_usage_blocks, stats.peak_usage_blocks, peak_percent);
#endif
        /*
         * The recommended way to establish the minimal size of the memory pool is to stress-test the application and
         * record the worst case memory usage.
         */
        if (peak_percent > 70)
        {
            puts("WARNING: ENLARGE MEMORY POOL");
        }
    }

    /*
     * Transmitting the node status message periodically.
     */
    {
        uint8_t buffer[UAVCAN_NODE_STATUS_MESSAGE_SIZE];
        makeNodeStatusMessage(buffer);

        static uint8_t transfer_id;

        const int bc_res = canardBroadcast(&canard, UAVCAN_NODE_STATUS_DATA_TYPE_SIGNATURE,
                                           UAVCAN_NODE_STATUS_DATA_TYPE_ID, &transfer_id, CANARD_TRANSFER_PRIORITY_LOW,
                                           buffer, UAVCAN_NODE_STATUS_MESSAGE_SIZE);
        if (bc_res <= 0)
        {
            printf("Could not broadcast node status; error %d\n", bc_res);
        }
    }

    /*{
        static uint8_t transfer_id;
        uint8_t payload[1];
        uint8_t dest_id = 1;
        const int resp_res = canardRequestOrRespond(&canard,dest_id,UAVCAN_GET_NODE_INFO_DATA_TYPE_SIGNATURE,
                                                    UAVCAN_GET_NODE_INFO_DATA_TYPE_ID,&transfer_id,
                                                    CANARD_TRANSFER_PRIORITY_LOW, CanardRequest,
                                                    payload, 0);
        if (resp_res <= 0)
        {
          printf("Could not request GetNodeInfo; error %d\n", resp_res);
        }
    } */

    node_mode = UAVCAN_NODE_MODE_OPERATIONAL;
}

/**
 * Transmits all frames from the TX queue, receives up to one frame.
 */
void processTxRxOnce(void)
{
    // Transmitting
    const CanardCANFrame* txf;
    for (txf = NULL; (txf = canardPeekTxQueue(&canard)) != NULL;)
    {
        const int tx_res = canardAVRTransmit(txf);
        if (tx_res < 0)         // Failure - drop the frame and report
        {
            canardPopTxQueue(&canard);
            printf("Transmit error %d, frame dropped\n", tx_res);
        }
        else if (tx_res > 0)    // Success - just drop the frame
        {
            canardPopTxQueue(&canard);
        }
        else                    // Timeout - just exit and try again later
        {
            break;
        }
    }

    // Receiving
    CanardCANFrame rx_frame;
    const uint64_t timestamp = getMonotonicTimestampUSec();
    const int rx_res = canardAVRReceive(&rx_frame);
    if (rx_res < 0)             // Failure - report
    {
        printf("Receive error %d\n", rx_res);
    }
    else if (rx_res > 0)        // Success - process the frame
    {
        canardHandleRxFrame(&canard, &rx_frame, timestamp);
    }
    else
    {
        ;                       // Timeout - nothing to do
    }
}

/****************************************************************************
* Public Functions
****************************************************************************/
int main()
{
    PORT_init();
    timer_init();
    usart_init();
    /*
     * Setup Fake Power values
     */
    power_circuit_status.voltage_mv_5V = 5000;
    power_circuit_status.voltage_mv_12V = 12020;
    power_circuit_status.voltage_mv_15V = 15001;
    power_circuit_status.voltage_mv_48V = 47999;
    power_circuit_status.current_ma_5V = 321;
    power_circuit_status.current_ma_12V = 423;
    power_circuit_status.current_ma_15V = 1122;
    power_circuit_status.current_ma_48V = 898;
    canardAVRInit(CAN_BITRATE);

    canardInit(&canard, canard_memory_pool, sizeof(canard_memory_pool),
               onTransferReceived, shouldAcceptTransfer);

    // bypass dynamic node allocation
    canardSetLocalNodeID(&canard, PREFERRED_NODE_ID);

    /*
     * Performing the dynamic node ID allocation procedure.
     */
    static const uint8_t PreferredNodeID = PREFERRED_NODE_ID;    ///< This can be made configurable, obviously

    node_id_allocation_unique_id_offset = 0;

    uint8_t node_id_allocation_transfer_id = 0;

    while (canardGetLocalNodeID(&canard) == CANARD_BROADCAST_NODE_ID)
    {
        puts("Waiting for dynamic node ID allocation...");

        send_next_node_id_allocation_request_at =
            getMonotonicTimestampUSec() + UAVCAN_NODE_ID_ALLOCATION_REQUEST_DELAY_OFFSET_USEC +
            (uint64_t)(getRandomFloat() * UAVCAN_NODE_ID_ALLOCATION_RANDOM_TIMEOUT_RANGE_USEC);

        while ((getMonotonicTimestampUSec() < send_next_node_id_allocation_request_at) &&
               (canardGetLocalNodeID(&canard) == CANARD_BROADCAST_NODE_ID))
        {
            processTxRxOnce();
        }

        if (canardGetLocalNodeID(&canard) != CANARD_BROADCAST_NODE_ID)
        {
            break;
        }

        // Structure of the request is documented in the DSDL definition
        // See http://uavcan.org/Specification/6._Application_level_functions/#dynamic-node-id-allocation
        uint8_t allocation_request[CANARD_CAN_FRAME_MAX_DATA_LEN - 1];
        allocation_request[0] = PreferredNodeID << 1;

        if (node_id_allocation_unique_id_offset == 0)
        {
            allocation_request[0] |= 1;     // First part of unique ID
        }

        static const uint8_t MaxLenOfUniqueIDInRequest = 6;
        uint8_t uid_size = (uint8_t)(UNIQUE_ID_LENGTH_BYTES - node_id_allocation_unique_id_offset);
        if (uid_size > MaxLenOfUniqueIDInRequest)
        {
            uid_size = MaxLenOfUniqueIDInRequest;
        }

        // Paranoia time
        assert(node_id_allocation_unique_id_offset < UNIQUE_ID_LENGTH_BYTES);
        assert(uid_size <= MaxLenOfUniqueIDInRequest);
        assert(uid_size > 0);
        assert((uid_size + node_id_allocation_unique_id_offset) <= UNIQUE_ID_LENGTH_BYTES);

        memmove(&allocation_request[1], &unique_id[node_id_allocation_unique_id_offset], uid_size);

        // Broadcasting the request
        const int bcast_res = canardBroadcast(&canard,
                                              UAVCAN_NODE_ID_ALLOCATION_DATA_TYPE_SIGNATURE,
                                              UAVCAN_NODE_ID_ALLOCATION_DATA_TYPE_ID,
                                              &node_id_allocation_transfer_id,
                                              CANARD_TRANSFER_PRIORITY_LOW,
                                              &allocation_request[0],
                                              (uint16_t) (uid_size + 1));
        if (bcast_res < 0)
        {
            (void)fprintf(stderr, "Could not broadcast dynamic node ID allocation request; error %d\n", bcast_res);
        }

        // Preparing for timeout; if response is received, this value will be updated from the callback.
        node_id_allocation_unique_id_offset = 0;
    }

    printf("Dynamic node ID allocation complete [%d]\n", canardGetLocalNodeID(&canard));

    canardAVRConfigureAcceptanceFilters(canardGetLocalNodeID(&canard));
    printf("Set hardware filter according to node id");

    uint64_t next_1hz_service_at = getMonotonicTimestampUSec();

    while(1)
    {
        //LED_TOGGLE(); // check timing!
        processTxRxOnce();

        const uint64_t ts = getMonotonicTimestampUSec();

        if (ts >= next_1hz_service_at)
        {
            next_1hz_service_at += FACTOR_1S;
            process1HzTasks(ts);
        }
    }

    return 0;
}
